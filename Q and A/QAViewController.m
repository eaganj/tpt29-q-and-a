//
//  QAViewController.m
//  Q and A
//
//  Created by James Eagan on 18/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import "QAViewController.h"

@interface QAViewController ()

@end

@implementation QAViewController

- (id)init
{
    self = [super initWithNibName:@"QAView" bundle:nil];
    if (!self)
        return self;
    
    // Custom initialization
    currentQuestion = -1;
    questions = [[NSArray alloc] initWithObjects:@"Who founded NeXT?",
                 @"What is your favorite color?",
                 @"What is the answer to the ultimate question?", nil];
    answers = @[@"Steve Jobs",
                @"Purple",
                @"42"];
//        int fortytwo = 42;
//        NSDictionary *aDict = @{@"key": @42, @"42": @(fortytwo)};
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)askQuestion:(id)sender {
    currentQuestion = (currentQuestion + 1) % questions.count;
//    self.questionLabel.text = [questions objectAtIndex:currentQuestion];
//    [self.questionLabel setText:[questions objectAtIndex:currentQuestion]];
    self.questionLabel.text = questions[currentQuestion];
    self.answerLabel.text = @"???";
//    NSLog(@"Question: %@ Label: %@", questions[currentQuestion], self.questionLabel.text);
    
}

- (IBAction)showAnswer:(id)sender {
    self.answerLabel.text = answers[currentQuestion];
}

@end
