//
//  QAAppDelegate.h
//  Q and A
//
//  Created by James Eagan on 18/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
