//
//  main.m
//  Q and A
//
//  Created by James Eagan on 18/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QAAppDelegate class]));
    }
}
