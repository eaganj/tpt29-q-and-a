//
//  QAViewController.h
//  Q and A
//
//  Created by James Eagan on 18/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QAViewController : UIViewController
{
    int currentQuestion;
    NSArray *questions;
    NSArray *answers;
}

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

- (id)init;
- (IBAction)askQuestion:(id)sender;
- (IBAction)showAnswer:(id)sender;

@end
